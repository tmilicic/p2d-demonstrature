//============================================================================
// Name        : main.cpp
// Author      : Tanja Miličić
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================

#include <iostream>
#include <fstream>
using namespace std;

//============================================================================
//					PRETRAZIVANJE
//============================================================================


// Razmislite kako bi binarno pretrazivanje izgledalo
// ukoliko bismo pretrazivali polje koje je sortirano
// silazno, a ne uzlazno.

int binarno(int polje[], int n, int elt) {
	int poc = 0;
	int kraj = n-1;
	int sred;

	while(poc <= kraj){
		sred = (poc+kraj)/2;
		if(elt <polje[sred]){
			kraj = sred - 1;
		}
		else if(elt>polje[sred]){
			poc = sred + 1;
		}
		else{
			return sred;
		}
	}

	return -1;
}

//============================================================================
//					SORTIRANJE
//============================================================================

void ispisInsertionsorta(int polje[], int n, int zid){
	for(int i=0; i<n; i++){
		if(i == zid){
			cout << "|";
		}
		cout << polje[i] << " ";
	}
}

void insertionSort(int niz[], int n) {
	int temp, j;

	for(int i=1; i<n; i++){
		temp = niz[i];
		j = i;

		while(j > 0 && temp < niz[j-1]){
			niz[j] = niz[j-1];
			j--;
		}
		niz[j] = temp;

		ispisInsertionsorta(niz, n, i);
		cout << endl;
	}
}

// Pokusajte ispisati korake shell sorta

void shellSort(int niz[], int n) {

	int temp,j;
	for(int k=n/2; k>=1; k/=2){
		for(int i=k; i<n; i++){
			temp = niz[i];
			j = i;
			while(j>=k && temp < niz[j-k]){
				niz[j] = niz[j-k];
				j-=k;
			}
			niz[j] = temp;
		}
	}
}

//============================================================================
//					REKURZIJA
//============================================================================

void ispisBrojevaOdNdo1(int n){

	if(n > 1){
		cout << n << " ";
		ispisBrojevaOdNdo1(n-1);
	}else{
		cout << 1;
	}

}

int potencija(int baza, int n){
	if(n == 1 || n == 0){
		return baza;
	}else{
		return baza * potencija(baza, n-1);
	}
}

//============================================================================
//					DATOTEKE
//============================================================================

void pisanjeUDatoteku(string filename, int polje[], int n){

	ofstream myfile(filename.c_str()); 
	// c_str() je potreban ukoliko otvaramo datoteku
	// cije je ime zapisano u varijabli

	if(myfile.is_open()){
		for(int i=0; i<n; i++){
			myfile << polje[i] << " ";
		}
		myfile.close();
	}
}

void citanjeIzDatoteke(string filename){
	ifstream myfile (filename.c_str());
	string line;
	if(myfile.is_open()){
		while(getline(myfile, line)){
			cout << line << " ";
		}

		myfile.close();
	}
}

void ispisPolja(int polje[], int n){
	for(int i=0; i<n; i++){
		cout << polje[i] << " ";
	}
}

int main() {

	int polje[] = {1,2,3,4,5,6,7,8,9,10};
	int index = binarno(polje, 10, 5);

	cout << "Poziv binarnog pretrazivanja za polje: " << endl;

	ispisPolja(polje, 10);

	cout << endl;

	if(index == -1){
		cout << "Element nije poronaden" << endl;
	}else{
		cout << "Element se nalazi na " << index << ". mjestu" << endl;
	}

	int nesortirano[] = {7,12,4,9,22,10,1,5};

	cout << "Poziv insertion sorta s ispisom koraka" << endl;
	cout << "Znak | odvaja nesortirani dio od sortiranog" << endl;

	insertionSort(nesortirano, 8);
	//shellSort(nesortirano, 8);

	cout << "Zapisivanje sortiranog polja u datoteku..." << endl;
	pisanjeUDatoteku("polje.txt", nesortirano, 8);

	cout << "Citanje polje iz datoteke" << endl;
	citanjeIzDatoteke("polje.txt");
	cout << endl;

	cout << "Rekurzivan ispis brojeva od 1 do N" << endl;
	ispisBrojevaOdNdo1(10);
	cout << endl;

	cout << "Rekurzivno izracunavanje 2^3" << endl;
	int rez = potencija(2, 3);
	cout << rez << endl;
	return 0;
}
